import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Result } from './result';

@Injectable()
export class SearchService {
  
  private searchURL = 'https://lrghflf9jg.execute-api.us-east-1.amazonaws.com/dev/search?search_key=';
  
  constructor(
    private http: Http
  ) {}

  getResult(search_key: string): Observable<Result[]> {
    return this.http.get(this.searchURL+search_key)
                    .map((response: Response) => <Result[]>response.json())
                    .catch(this.handleError);
  }


  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}