import { Component, OnInit, ViewChild } from '@angular/core';

import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { SearchService } from './search.service';

import { Result } from './result';


@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css'],
  providers: [ SearchService ]
})
export class SearchResultComponent implements OnInit {

	
	results: Result[];
	errorMessage: string;

	@ViewChild('searchForm') searchForm: NgForm;

  	constructor(private searchService: SearchService) { }

  	ngOnInit() {
  		
  	}

  	onSubmitSearch(){
  		console.log('### onSubmitSearch ');
  		console.log("## onSubmitSearch onSubmitAlias this.searchForm.value.search_key: ", this.searchForm.value.search_key);

  		this.searchService.getResult(this.searchForm.value.search_key)
				.subscribe(
					results => this.results = results,
					error => this.errorMessage = <any>error
				);
  		
  	}

  	

}
